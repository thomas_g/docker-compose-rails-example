require 'open-uri'

class PagesController < ApplicationController

  def landing
  end

  def test_api
    @api_result = open('http://foo.example/api').read
  end
end
