# Rails Docker compose development example

This is a development setup for web applications using docker-compose. This example runs 2 Ruby on Rails applications that communicate with each other using domain names.
To achieve this it uses the jwilder/nginx-proxy docker image, leting us set VIRTUAL_HOST variables in our docker-compose file to setup nginx servers.
There is also a Dnsmasq container that is used to resolve wildcard domains (see https://alexanderzeitler.com/articles/accessing-an-api-in-docker-on-macbook-from-ios-iphone-ipad-using-dnsmasq/).
Finaly volumes are used for mounting the code to run and for persisting both Postgres datas and bundler gems.

## Overview

Service     | Description                     | Notes/Comments
------------|---------------------------------|---------------
nginx-proxy | Reverse proxy                   | localhost:80
dns         | Dnsmasq                         | localhost:5380
postgres    | Postgres 10.5 database          | postgres:5432 (internally)
foo         | Ruby on Rails 5.1.4, ruby 2.5.1 | foo.example
bar         | Ruby on Rails 5.1.4, ruby 2.5.1 | bar.example

## Setup

### Prerequisites

1.  Docker
2.  Docker Compose

### Clone the project

```
$ git clone https://...
$ cd ...
```

### DNS setup

- Edit volumes/dnsmasq.conf and set your ip address
- Use your ip address as DNS server

Exemple on macOs
```
$ networksetup -setdnsservers Wi-Fi 192.168.0.x 1.0.0.1 1.1.1.1
```

## Run

```
$ docker-compose run foo bundle install
$ docker-compose run bar bundle install
$ docker-compose up
$ docker-compose run foo bundle exec rails db:create
$ docker-compose run foo bundle exec rails db:migrate
```

- Visit http://foo.example/albums and create persisted data.
- Visit http://bar.foo.example/ and perform a http call.

## Useful commands

```
# start the containers rebuilding images
$ docker-compose up --build

# show images
$ docker images

# show containers
$ docker-compose ps

# run a rails command
$ docker-compose run foo bundle exec rails console

# ssh inside container
$ docker-compose exec <container_id> sh
```
